This repository has been built using [jadegit](https://gitlab.com/jadelab/jadegit), using the source from the [original project on github](https://github.com/jadelab/AutomatedTestSchema).

The latest generated schemas built using jadegit can be downloaded from [here](https://gitlab.com/jadelab/automatedtestschema/-/jobs/artifacts/master/download?job=AutomatedTestSchema).